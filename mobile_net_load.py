import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt

loaded_model = tf.keras.models.load_model('mobilenet_DVC.h5')

enter_list = input('Enter a list of number from 1 to 25000, sperating by a semi-colon: ')

img_list =  enter_list.split(';')

img_array_lists = []
input_shape = (160, 160, 3)

for img_file in img_list:
    full_path = 'test1/' + str(img_file) + '.jpg'
    img = tf.keras.preprocessing.image.img_to_array(tf.keras.preprocessing.image.load_img(full_path, color_mode='rgb', target_size=input_shape))
    img = (img / 127.5) - 1
    img_array_lists.append(img)

img_array_lists = np.array(img_array_lists)

predictions = loaded_model.predict(img_array_lists)
my_dict = { 0 : 'Dog', 1 : 'Cat'}

cols = 3
if len(img_list) % 3 == 0:
    rows = int(len(img_list) / 3)
else:
    rows = int(len(img_list) / 3) + 1

fig = plt.figure()
for num in range(predictions.shape[0]):
    max_score = np.argmax(predictions[num], axis = 0)
    pred_label = my_dict.get(max_score)
    fig.add_subplot(rows, cols, num + 1).set_title(pred_label)
    img = plt.imread('test1/{}.jpg'.format(num+1))
    plt.axis('off')
    plt.imshow(img)

fig.tight_layout()
plt.show()
    
print('Finished!')