import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow as tf
import numpy as np
from tensorflow.keras.utils import to_categorical
# phân loại chó mèo với pre-trained model mobilenet_v2
# xử lý dữ liệu
dog_path = 'dog.'
cat_path = 'cat.'

input_shape = (160, 160, 3)
img_path = 'train/'
number_of_imgs = 12500

img_stores = []
animals = []
order = list(np.random.permutation(number_of_imgs))

for num in order:
    print('Pre-processing image: ' + str(num))
    #DOG
    img = tf.keras.preprocessing.image.img_to_array(tf.keras.preprocessing.image.load_img(img_path + dog_path + str(num) + '.jpg', color_mode='rgb', target_size=input_shape))
    img = (img/127.5) - 1
    img_stores.append(img)
    animals.append(0)
    #CAT
    img = tf.keras.preprocessing.image.img_to_array(tf.keras.preprocessing.image.load_img(img_path + cat_path + str(num) + '.jpg', color_mode='rgb', target_size=input_shape))
    img = (img / 127.5) - 1
    img_stores.append(img)
    animals.append(1)
    print('Done image ' + str(num) + '\nNext image is ready!')
print('Done all!\nPrepare model!')

num_train = number_of_imgs * 2 * 0.8

X_train = (np.array(img_stores[:num_train]))
y_train = to_categorical((animals[:num_train]), 2)
X_val = (np.array(img_stores[num_train:]))
y_val = to_categorical(np.array(animals[num_train:]), 2)

# khởi tạo base_model
base_model = tf.keras.applications.MobileNetV2(input_shape=input_shape, include_top=False, weights='imagenet')
base_model.trainable = False

globalAveragePooling2D = tf.keras.layers.GlobalAveragePooling2D()
dense = tf.keras.layers.Dense(2)

model = tf.keras.models.Sequential([
    base_model, 
    globalAveragePooling2D,
    dense
])

model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

model.summary()

batch_size = 64
epochs = 50

history = model.fit(X_train, y_train, validation_data=(X_val, y_val), batch_size=batch_size, epochs=epochs)
print('Saving!')
model.save('mobilenet_DVC.h5')
print('Done!')